<?php

namespace DestiniaPruebaTecnica\Config;

class Config
{
    private static $instance;
    private $vars;
 
    private function __construct()
    {
        $this->vars = array();
    }
 
    public function get($name)
    {
        if(isset($this->vars[$name]))
        {
            return $this->vars[$name];
        }
    }
    
    public function set($name, $value)
    {
        if(!isset($this->vars[$name]))
        {
            $this->vars[$name] = $value;
        }
    }
 
    public static function singleton()
    {
        if (!isset(self::$instance)) {
            $class = __CLASS__;
            self::$instance = new $class;
        }
 
        return self::$instance;
    }
}