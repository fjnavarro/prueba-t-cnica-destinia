<?php

namespace DestiniaPruebaTecnica\Config;

require_once __DIR__.'/config.php';

$config = Config::singleton();
$config->set('driver', 'mysql');
$config->set('host', 'localhost');
$config->set('user', 'root');
$config->set('pass', 'root');
$config->set('database', 'destinia_prueba_tecnica');
$config->set('charset', 'UTF8');