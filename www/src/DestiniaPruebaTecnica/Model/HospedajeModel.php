<?php

namespace DestiniaPruebaTecnica\Model;

class HospedajeModel
{
    static public function findHospedajes($name)
    {
        require_once __DIR__.'/../Config/db.php';
        
        try{
            $name = substr($name,0,3);
            $conn = new \PDO("{$config->get('driver')}:host={$config->get('host')};dbname={$config->get('database')};charset={$config->get('charset')}",
                    $config->get('user'),
                    $config->get('pass'));
            
            $sql = $conn->prepare('
                SELECT
                    h.nombre AS nombre,
                    h.tipo AS tipo,
                    ht.tipo_habitacion_estandar AS tipo_habitacion_estandar,
                    ht.numero_estrellas AS numero_estrellas,
                    a.n_apartamentos_disponible AS n_apartamentos_disponible,
                    a.n_adultos_capacidad AS n_adultos_capacidad,
                    l.nombre AS localidad,
                    p.nombre AS provincia
                FROM hospedaje AS h
                    LEFT JOIN hotel AS ht 
                    ON h.id=ht.hospedaje_id
                    LEFT JOIN apartamento AS a 
                    ON h.id=a.hospedaje_id
                    LEFT JOIN localidad AS l 
                    ON h.localidad_id=l.id
                    LEFT JOIN provincia AS p 
                    ON l.provincia_id=p.id
            WHERE h.nombre LIKE :Nombre ORDER by h.nombre');
            
            $sql->execute(array(':Nombre' => "%{$name}%"));
            $resultado = $sql->fetchAll();
        }catch(PDOException $e){
            die("Falló la conexión: {$e->getMessage()}");
        }

        return $resultado;
    }
}
