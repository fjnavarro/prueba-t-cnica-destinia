<?php

namespace DestiniaPruebaTecnica\Controller;
use DestiniaPruebaTecnica\Model as Model;

class DefaultController
{
    public function indexAction()
    {
        return $this->view('index', array());
    }
    
    public function findAction()
    {
        $txtBuscar = isset($_POST['txtBuscar'])?$_POST['txtBuscar']:'';
        
        if(empty($txtBuscar)){
            return $this->indexAction();
        }
        
        $resultados = Model\HospedajeModel::findHospedajes($_POST['txtBuscar']);
        
        return $this->view('find', array(
            'resultados' => $resultados
        ));
    }
    
    public function view($vista, $datos){
        foreach ($datos as $id_assoc => $valor) {
            ${$id_assoc}=$valor;
        }
        
        require_once "../src/DestiniaPruebaTecnica/View/$vista.php";
    }
}