<?php
use DestiniaPruebaTecnica\Controller;

require_once __DIR__.'/../src/DestiniaPruebaTecnica/Config/loader.php';
$response =  new Controller\DefaultController();
$controller = isset($_GET["controller"])?$_GET["controller"]:'index';

switch($controller){
    case 'find':
        $response->findAction();
        break;
    default:
        $response->indexAction();
        break;
}