-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 29, 2015 at 11:16 PM
-- Server version: 5.5.34
-- PHP Version: 5.4.35-1+deb.sury.org~precise+1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `destinia_prueba_tecnica`
--

-- --------------------------------------------------------

--
-- Table structure for table `apartamento`
--

CREATE TABLE IF NOT EXISTS `apartamento` (
  `hospedaje_id` int(11) NOT NULL,
  `n_apartamentos_disponible` int(1) NOT NULL,
  `n_adultos_capacidad` int(1) NOT NULL,
  UNIQUE KEY `hospedaje_id` (`hospedaje_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `apartamento`
--

INSERT INTO `apartamento` (`hospedaje_id`, `n_apartamentos_disponible`, `n_adultos_capacidad`) VALUES
(2, 10, 4),
(5, 50, 6);

-- --------------------------------------------------------

--
-- Table structure for table `hospedaje`
--

CREATE TABLE IF NOT EXISTS `hospedaje` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `localidad_id` int(11) NOT NULL,
  `tipo` enum('Hotel','Apartamentos') COLLATE utf8_unicode_ci NOT NULL,
  `nombre` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ciudad_id` (`localidad_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `hospedaje`
--

INSERT INTO `hospedaje` (`id`, `localidad_id`, `tipo`, `nombre`) VALUES
(1, 1, 'Hotel', 'Azul'),
(2, 2, 'Apartamentos', 'Beach'),
(3, 4, 'Hotel', 'Blanco'),
(4, 3, 'Hotel', 'Rojo'),
(5, 5, 'Apartamentos', 'Sol y playa');

-- --------------------------------------------------------

--
-- Table structure for table `hotel`
--

CREATE TABLE IF NOT EXISTS `hotel` (
  `hospedaje_id` int(11) NOT NULL,
  `tipo_habitacion_estandar` enum('doble','doble con vistas','doble con hidromasaje','sencilla') COLLATE utf8_unicode_ci NOT NULL,
  `numero_estrellas` int(1) NOT NULL,
  UNIQUE KEY `hospedaje_id` (`hospedaje_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `hotel`
--

INSERT INTO `hotel` (`hospedaje_id`, `tipo_habitacion_estandar`, `numero_estrellas`) VALUES
(1, 'doble con vistas', 3),
(3, 'doble', 4),
(4, 'sencilla', 3);

-- --------------------------------------------------------

--
-- Table structure for table `localidad`
--

CREATE TABLE IF NOT EXISTS `localidad` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `provincia_id` int(11) NOT NULL,
  `nombre` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `provincia_id` (`provincia_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `localidad`
--

INSERT INTO `localidad` (`id`, `provincia_id`, `nombre`) VALUES
(1, 1, 'Valencia'),
(2, 2, 'Almería'),
(3, 3, 'Sanlucar'),
(4, 2, 'Mojacar'),
(5, 4, 'Málaga');

-- --------------------------------------------------------

--
-- Table structure for table `pais`
--

CREATE TABLE IF NOT EXISTS `pais` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `pais`
--

INSERT INTO `pais` (`id`, `nombre`) VALUES
(1, 'España');

-- --------------------------------------------------------

--
-- Table structure for table `provincia`
--

CREATE TABLE IF NOT EXISTS `provincia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pais_id` int(11) NOT NULL,
  `nombre` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pais_id` (`pais_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `provincia`
--

INSERT INTO `provincia` (`id`, `pais_id`, `nombre`) VALUES
(1, 1, 'Valencia'),
(2, 1, 'Almería'),
(3, 1, 'Cádiz'),
(4, 1, 'Málaga');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `apartamento`
--
ALTER TABLE `apartamento`
  ADD CONSTRAINT `apartamento_ibfk_1` FOREIGN KEY (`hospedaje_id`) REFERENCES `hospedaje` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `hospedaje`
--
ALTER TABLE `hospedaje`
  ADD CONSTRAINT `hospedaje_ibfk_1` FOREIGN KEY (`localidad_id`) REFERENCES `localidad` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `hotel`
--
ALTER TABLE `hotel`
  ADD CONSTRAINT `hotel_ibfk_1` FOREIGN KEY (`hospedaje_id`) REFERENCES `hospedaje` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `localidad`
--
ALTER TABLE `localidad`
  ADD CONSTRAINT `localidad_ibfk_1` FOREIGN KEY (`provincia_id`) REFERENCES `provincia` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `provincia`
--
ALTER TABLE `provincia`
  ADD CONSTRAINT `provincia_ibfk_1` FOREIGN KEY (`pais_id`) REFERENCES `pais` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
